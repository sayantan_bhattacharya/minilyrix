# Minilyrix
Minilyrix is a program inspired by the Minilyrics software available as a plugin to be used along with the media players in Windows systems.
This program, as opposed to showing the lyrics in the top portion of the screen or anywhere in that matter, will be showing the lyrics in a ncurses based window opened along with a text based player, for example cmus.

# Installation instructions
In order to run this program, please install the following extra xterm based music player

+ Cmus : Cmus is a terminal based music player which could be installed by issuing the command `sudo apt-get install cmus`. For other Nix flavours, just google it, or request an update to the author of this program and the same should be updated. Please note that cmus seems to use libdnet libraries and may show the error of `Can not open /etc/decnet.conf`. Some have suggested that installing cmus using the `--no-install-recommends` did the trick as mentioned [here](https://bugs.launchpad.net/ubuntu/+source/cmus/+bug/923027). It seems the bug fix for the same has been released but has been mentioned here because one of the testers had an issue with 15.10.

+ After installing the cmus terminal based program, clone this repository. Change the pwd to the cloned directory and then run `make clean && make`. The program should get compiled and the executable will be present in the `bin' directory under the cloned directory.

+ Please create a directory by the name of `lyrics` and place it inside the `bin` directory. The directory structure should look as follows:
```
|-- bin
|   |-- lyrics
|   |-- main0
|   `-- tempFile
|-- contributors.txt
|-- inc
|   |-- global.h
|   |-- main.h
|   |-- mlxEdit.h
|   `-- mlx.h
|-- Makefile
|-- obj
|   |-- global.o
|   |-- main.o
|   |-- mlxEdit.o
|   `-- mlx.o
|-- README.md
|-- src
|   |-- global.c
|   |-- main.c
|   |-- mlx.c
|   `-- mlxEdit.c
`-- workspace

```

+ Please place all the lyrics files with the extension of `lrc` preceeded by the filename of the file being played by Minilyrix in the lyrics directory, Minilyrix will pick up the file based on the filename being played and display the contents provided the time tagging is present. If not, the same can be tagged with the edit mode in Minilyrix.

# Working modes
Minilyrix has two modes, by default it is in the lyrics display mode.

# Pre-requisites
+ Make(Development version : 3.81)
+ Cmus(As available from the software sources
+ cmus-remote(This program uses this one heavily to show the lyrics properly)
+ GNU C Compiler tool chain(GCC version used during development : gcc-5)

As just a note, please check if awk and sed is present too or not, it should be, but the author of this software is not too sure.

# Media Players supported as of now
+ Cmus

# Changelog
+ Removed time stamp from lyrics display[31/5]
+ Dynamic file name and path processing[01/06]
+ Clear screen on file change while playing music[17/07]
+ kbhit() error resolution and event monitoring fix[14/08]
+ Fixed all issues in tagging and checked in Beta version[25/10]

Please feel free to make suggestions for code changes and errors in coding standards.
