#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>

/* (w) Will contain only global functions that will be used */
int kbhit(void);    /* (w) Poll the keyboard for any key press */

#endif
