#ifndef _MLX_H
#define _MLX_H

#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* (w) Including custom header */
#include "mlxEdit.h"
#include "global.h"

/* (w) Macros */
#define MAXLEN 1035

/* (w) Commands to be run */
#define TIME_ELAPSED "cmus-remote -Q | grep -E \"position\" | grep -Eo \"[0-9]*\" | sed -e \'s/^\\s*//\' -e \'/^$/d\' | tr -d \'\\n\'"
#define FILE_PLAYED "cmus-remote -Q | grep -E \"file\" | awk -F \"/\" '{print $NF}' | rev | awk '{print substr($0, 4)}' | rev | tr -d '\\n'"

/* (w) functions */
void startMlx(void);
void runXtermCmdSpc(char *inpCommand);
void displayOutput(); /* (w) refresh after reading the lyrics file */
void readFile(); /* (w) Lyrics file reader function */
pid_t getPid(); /* (w) this is not getpid() as mentioned in the linux man pages */

#endif
