#ifndef _MLXEDIT_H
#define _MLXEDIT_H

#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include "global.h"

#define INVALID -2
#define MAXLINE 1000
#define LRCRD 1
#define LRCWR 2
#define SPECMODE 3
#define REFRESHMDOE 4
#define TEMP_FILE "./tempFile"
#define CURTIME "cmus-remote -Q | grep -E \"position\" | grep -Eo \"[0-9]*\" | sed -e \'s/^\\s*//\' -e \'/^$/d\' | tr -d \'\\n\'"
#define LINELEN 1000

/* (w) global variables */
int printEdPosX, printEdPosY;
int lindex;

/* (w) Editor functions */
void startEditor(char *inpFileName, int mode);
void _readLrcFile(char *inpFileName); /* (w) This will be displaying the whole content of the file in the stdscr - internal function, cannot be called directly */
void _editLrcFile(char *inpFileName);
void _moveCursor(int posx, int posy);
void _getTime(char *inpCommand);

/* (w) other utility functions for moving the cursor on the screen */
void incLine(); /* (w) move cursor one line down */
void decLine(); /* (w) move cursor one line up */

#endif
