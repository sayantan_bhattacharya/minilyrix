/*
 * =====================================================================================
 *
 *       Filename:  global.c
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Sunday 09 August 2015 12:53:12  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/global.h"

int kbhit(void)
{
    /* (w) poll the keyboard */
    int keypressed = getch();

    if(keypressed != ERR)
    {
        ungetch(keypressed);
        return 1;
    }
    else
        return 0;
}
