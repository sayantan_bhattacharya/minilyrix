/*
 * =====================================================================================
 *
 *       Filename:  mlx.c
 *
 *    Description:  Minilyrix lyrics display code
 *
 *        Version:  1.0
 *        Created:  Sunday 24 May 2015 04:48:07  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/mlx.h"

/* (w) DONE : write the function body for the lyrics file reading
 * (w) DEFERRED : create the respective directories for the lyrics files
 * (w) DONE : Only show lyrics without the time stamps
 * (w) DONE : Check the name of the file being played
 * (w) DONE : Check if the song being played has changed or not
 * (w) DONE : Create check to see if the cmus program is running or not
 * (W) DEFERRED : Show the lyrics file not found at the top of the screen only once
 * (w) DEFERRED : Show found lyrics file at the top of the ncurses window only once
 * (w) DONE : Clear the screen when the lyrics file has been found and then display the lyrics
 * (w) DONE : Write the lyrics tagger/editor for Minilyrix
 * (w) DONE : Add a keyhandler for launching the Minilyrix lyrics editor */

/* (w) Global variables - not sure whether I should be using them */
char cmdOut[MAXLEN];
char fileName[MAXLEN];
char prevFileName[MAXLEN], curFileName[MAXLEN]; /* (w) for checking the filename that was present/ have been playing */
char *lrcLine = NULL;
bool fileChanged = false;
bool showOnce = false;
bool calledFirst = false; /* (w) Assuming that the call was not done first */

/* (w) mode for the minilyrix part */
typedef enum {LRCDISP, LRCTAG} MlxMode;
MlxMode mode = LRCDISP; /* (w) By default the mode would be set to displaying the lyrics, even if the lyrics file is not present */

/* (w) start implementing the main program written for the minilyrix project */
void startMlx(void)
{
    /* (w) Before even starting the ncurses, check if the music player is running or not */
    initscr();
    noecho();
    cbreak();
    curs_set(true);
    pid_t startedPid = 0; /* (w) Assuming that CMUS has not been started */
    fileChanged = false; /* (w) Assuming the file being played has not yet changed, if changed, read the lyrics file of the new song */

    /* (w) Setup the non blocking portion of the display */
    nodelay(stdscr, TRUE);
    scrollok(stdscr, TRUE);

    if(getPid() == 0)
    {
        printw("CMUS is not running, please make sure a song is initially being played when CMUS is on");
        refresh();
        getch();
        endwin();
        exit(0);
    }
    else /* (w) If started, just store the PID */
        startedPid = getPid();

    /* (w) first get the name of the file that needs to be played - change the extension of the file so that mlx can easily find it */
    strcat(fileName, "./lyrics/");

    runXtermCmdSpc(FILE_PLAYED);
    strcat(fileName, "lrc");

    printw("Lyics file : %s\n", fileName);

    /* (w) update the previous file name with the current file name */
    strcpy(prevFileName, fileName);

    /* (w) check the new function written for shell command running */
    runXtermCmdSpc(TIME_ELAPSED);

    while(1)
    {
        /* (w) Now I need to check if the keyboard is being hit or not - non
         * blocking settings doesn't seem to stop the lyrics display though*/
        if(kbhit())
        {
            int keyVal = getch();
            if((keyVal == 'e' || keyVal == 'E') && mode == LRCDISP)
            {
                mode = LRCTAG;
                showOnce = true;
            }
            else if((keyVal == 'w' || keyVal == 'W') && mode == LRCTAG)
            {
                mode = LRCDISP;
                showOnce = true;
                calledFirst = false; /* (w) reset the called first boolean because in the next call the function needs to run */
            }
            else if(keyVal == 'q' || keyVal == 'Q')
            {
                /* (w) exit the program */
                clear();
                printw("Exiting Minilyrix program\nHit any key to exit");
                refresh();
                getch();
                endwin();
                exit(0);
            }
            else if((keyVal == 'j' || keyVal == 'J') && mode == LRCTAG)
            {
                /* (w) move the cursor down to the next line */
                incLine();
                refresh();
            }
            else if((keyVal == 'k' || keyVal == 'K') && mode == LRCTAG)
            {
                /* (w) function call will be added shortly */
                decLine();
                refresh();
            }
            else if((keyVal == 't' || keyVal == 'T') && mode == LRCTAG)
            {
                /* (w) first check if the file has been changed or not */
                startEditor(fileName, LRCWR);
                refresh();
            }
            else if((keyVal == 'r' || keyVal == 'R') && mode == LRCTAG)
            {
                startEditor(fileName, REFRESHMDOE); /* (w) re-write something was written on the screen */
                refresh(); /* (w) This is just to refresh the screen when tagging in case something else gets printed in the screen */
            }
        }

        if(startedPid != getPid())
        {
            /* (w) Assuming that the music player instance opened will be once and that the music player if exited will also result in the stoppage of this program */
            printw("CMUS has stopped in the midst of playing music\nThis program will now shut down\nHit any key to exit\n");
            refresh();
            getch();
            endwin();
            exit(0);
        }

        /* (w) Assuming that anything that is below this line is running for the lyrics display portion */

        /* (w) reset the curFileName string array */
        memset(fileName, 0, MAXLEN);
        strcat(fileName, "./lyrics/");

        if(mode == LRCDISP)
        {
            if(showOnce)
                clear();
            showOnce = false;
            runXtermCmdSpc(FILE_PLAYED);
            strcat(fileName, "lrc");

            /* (w) Now start the comparison of the filenames */
            if(strcmp(fileName, prevFileName) == 0);
            else
            {
                /* (w) File has changed, change the previous file name */
                memset(prevFileName, 0, MAXLEN);
                strcpy(prevFileName, fileName);
                fileChanged = true;
            }

            /* (w) reload */
            runXtermCmdSpc(TIME_ELAPSED);
            displayOutput();
            sleep(1); /* (w) because the time is updated every second */
            refresh();
        }
        else if(mode == LRCTAG)
        {
            if(showOnce)
                clear(); /* (w) this will have to removed after the main calls have been done */
            showOnce = false;
            runXtermCmdSpc(FILE_PLAYED);
            strcat(fileName, "lrc");

            if(!calledFirst)
            {
                startEditor(fileName, SPECMODE); /* (w) call this function only when tagging event has occurred or file has to be shown for the first time when tagging mode entered */
                /* (w) this portion should handle the showing of the file when the tagging mode has been entered for the first time */
                calledFirst = true;
            }
            refresh();
        }
    }

    //getch(); /* (w) hold the screen before closing it off */
    endwin();
}

void runXtermCmdSpc(char *inpCommand)
{
    FILE *pipeFHandler;
    char tempOut[MAXLEN];

    /* (w) this function will try to run the specified command and return the result - over-loaded from the void param type of runXtermCmd */
    pipeFHandler = popen(inpCommand, "r"); /* (w) only use the read mode of the process open function */
    if(pipeFHandler == NULL)
        printw("Could not run command : %s", inpCommand);
    else
        fgets(tempOut, sizeof(tempOut), pipeFHandler);

    /* (w) check which string has been provided to the function */
    if(strcmp(inpCommand, TIME_ELAPSED) == 0)
        strcpy(cmdOut, tempOut);
    else if(strcmp(inpCommand, FILE_PLAYED) == 0)
        strcat(fileName, tempOut); /* (w) the else if portion of the code will be added shortly */

    pclose(pipeFHandler);
}

void displayOutput()
{
    /* (w) First read the file and then refresh the screen */
    readFile();
    refresh();
}

void readFile()
{
    size_t lenLrcLine = 0;
    ssize_t readLine = 0;
    size_t iter;

    int defScrX, defScrY;

    FILE *fPtr;

    /* (w) make changes to the fileName before opening the file and reading the same */
    fPtr = fopen(fileName, "r");
    if(fPtr == NULL)
    {
        if(fileChanged)
        {
            clear();
            printw("File : %s not found\n", fileName);
            fileChanged = false;
        }
        return;
    }
    else
    {
        if(fileChanged)
            clear();
        fileChanged = false;
    }

    /* (w) get the screen size first */
    getmaxyx(stdscr, defScrY, defScrX);

    /* (w) Append the time stamp in the line */
    strcat(cmdOut, ":");
    int printPosY, printPosX;
    printPosY = 0;
    printPosX = defScrX;

    /* (w) start the processing of the file */
    while((readLine = getline(&lrcLine, &lenLrcLine, fPtr)) != EOF)
    {
        if(strstr(lrcLine, cmdOut) != NULL)
            if((strstr(lrcLine, cmdOut) - lrcLine) == 0)    /* (w) Check if the time stamp index is at 0 or not */
                if(strchr(lrcLine, ':') != NULL) /* (w) If the : character is found then... */
                    for(iter = (strchr(lrcLine, ':') - &lrcLine[0] + 1); iter < strlen(lrcLine); ++iter)
                    {
                        mvprintw(printPosY, (printPosX - strlen(lrcLine) + iter) / 2, "%c", lrcLine[iter]); /* (w) Only the line, not the time stamp */
                        printPosX++;
                    }
        refresh();
        printPosY++;
    }

    free(lrcLine);
}

pid_t getPid()
{
    char tempOut[MAXLEN];

    FILE *cmd = popen("pidof cmus", "r");
    fgets(tempOut, sizeof(tempOut), cmd);

    pid_t pid = strtoul(tempOut, NULL, 10);
    //printw("Pid of : %d", pid); /* (w) Logger statement to check if the program is working */
    pclose(cmd);

    return pid;
}
