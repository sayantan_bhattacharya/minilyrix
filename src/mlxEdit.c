/*
 * =====================================================================================
 *
 *       Filename:  mlxEdit.c
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  Sunday 19 July 2015 12:41:10  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/mlxEdit.h"

/* (w) required global variables */
char *lrcEdLine= NULL;
extern int printEdPosX;
extern int printEdPosY;
extern int lindex;
int yvalues[MAXLINE]; /* (w) maximum number of lines allowed for a lyrics file is 1000 at the moment */
char cmdOut[MAXLINE];

/* (w) TODO : Display the lyrics file contents in the middle of the screen
 * (w) TODO : Cursor movement needs to be placed - j for down and k for up */

/* (w) Editor functions */

void startEditor(char *inpFileName, int mode)
{
    /* (w) read the lyrics file for tagging only when the file has been changed, else if
     * started for the first time, just read once and then position the cursor at the start of the
     * first line */
    int tempYpos, tempIndex;

    switch(mode)
    {
        case LRCRD:
            /* (w) mode to read the file */
            _readLrcFile(inpFileName);
            break;

        case LRCWR:
            /* (w) mode to call the function to write the lyrics file */
            _editLrcFile(inpFileName);
            tempYpos = printEdPosY;
            tempIndex = lindex;
            _readLrcFile(inpFileName); /* (w) refresh the screen and re-read the file */
            move(++tempYpos, yvalues[++tempIndex]);
            printEdPosY = tempYpos;
            lindex = tempIndex;
            refresh();
            break;

        case REFRESHMDOE:
            tempYpos = printEdPosY;
            tempIndex = lindex;
            _readLrcFile(inpFileName);
            printEdPosY = tempYpos;
            lindex = tempIndex;

            /* (w) Place the cursor at the right place */
            move(printEdPosY, yvalues[lindex]);
            refresh();
            break;

        case SPECMODE:
            _readLrcFile(inpFileName);
            printEdPosX = yvalues[0];
            printEdPosY = 1;
            lindex = 0;
            _moveCursor(printEdPosX, printEdPosY);
            break;

        default:
            break;
    }
}


void incLine()
{
    /* (w) first check the global index */
    if(yvalues[lindex] != INVALID && lindex < MAXLINE)
    {
        lindex++;
        printEdPosY++;
    }

    /* (w) reset */
    if(yvalues[lindex] == INVALID || lindex >= MAXLINE)
    {
        lindex = 0;
        printEdPosY = 1;
    }
    move(printEdPosY, yvalues[lindex]);
}

void decLine()
{
    if(yvalues[lindex] != INVALID && lindex > 0)
    {
        lindex--;
        printEdPosY--;
    }

    /* (w) reset */
    if(yvalues[lindex] == INVALID || lindex < 0)
    {
        lindex = 0; /* (w) Have to check this portion of the code */
        printEdPosY = 1;
    }
    move(printEdPosY, yvalues[lindex]);
}

void _readLrcFile(char *inpFileName)
{
    clear();

    printw("Reading lyrics file : %s\n", inpFileName);

    /* (w) Now read the file and display the contents in the editor portion */
    size_t lenEdLrcLine = 0;
    ssize_t readEdLine = 0;
    FILE *fEdPtr = NULL;

    /* (w) position vars */
    int defEdScrX, defEdScrY;

    fEdPtr = fopen(inpFileName, "r");
    if(fEdPtr == NULL)
    {
        printw("%s file not found, please check the lyrics directory\n", inpFileName);
        return;
    }

    /* (w) get the currrent cursor location */
    getmaxyx(stdscr, defEdScrY, defEdScrX);
    defEdScrY = 1;

    /* (w) poopulate the array with invalid information for start */
    for(lindex = 0; lindex < MAXLINE; lindex++)
        yvalues[lindex] = INVALID;

    /* (w) if the file is found, read the contents and show it in the screen */
    lindex = 0; /* (w) reset the index value */
    while((readEdLine = getline(&lrcEdLine, &lenEdLrcLine, fEdPtr)) != EOF)
    {
        /* printw("%s", lrcEdLine); */
        mvprintw(defEdScrY, (defEdScrX - strlen(lrcEdLine)) / 2, "%s", lrcEdLine);
        yvalues[lindex] = (defEdScrX - strlen(lrcEdLine)) / 2;
        defEdScrY++;
        if(lindex < MAXLINE)
            lindex++;
    }

    fclose(fEdPtr);
    free(lrcEdLine);
}

void _moveCursor(int posx, int posy)
{
    move(posy, posx); /* (w) hope this one works out */
}

void _editLrcFile(char *inpFileName)
{
    /* (w) Algorithm:
     * Open the lyrics file, check the lindex when the t button is pressed
     * Open a temp file, change the appropriate line aond write in a temp file
     * Read the temp file and then write the lines in the main lyrics file once again
     * Delete the temp file */

    /* (w) variables for opening the lrc file */
    size_t lenEdLrcLine = 0;
    ssize_t readEdLrcLine = 0;
    FILE *inpLrcFile = NULL; /* (w) Will be opening up the inpFileName provided */
    char *lrcLine = NULL; /* (w) reads from the file that is opened by inpLrcFile */
    char tempLrcLine[LINELEN]; /* (w) Need to change this one to a macro later */

    /* (w) variables for opening the temporary file */
    size_t lenTempLine = 0;
    ssize_t readTempLine = 0;
    FILE *tempFile = NULL;
    char *tempLine = NULL;

    /* (w) Open the lyrics file in read mode and the temp file in write mode */
    inpLrcFile = fopen(inpFileName, "r");
    tempFile = fopen(TEMP_FILE, "w");

    /* (w) check if both the files have been opened or not */
    if(inpLrcFile == NULL || tempFile == NULL)
    {
        printw("Either the tempFile or the lyrics file could not be opened");
        refresh();
        endwin();
        exit(2);
    }

    /* (w) First read the lyrics file, append the time tag at the start of the line and write it in the temp file */
    int lineCounter = 0; /* (w) Need to check the issue of previous tags to be present */

    /* (w) Before even starting to read the file, check the time */
    _getTime(CURTIME);
    while((readEdLrcLine = getline(&lrcLine, &lenEdLrcLine, inpLrcFile)) != EOF)
    {
        /* (w) first check if the lindex */
        if(lineCounter == lindex)
        {
            /* (w) Concatenate the time frame returned from the player to the line and write it */
            strcpy(tempLrcLine, cmdOut);
            strcat(tempLrcLine, ":");
            strcat(tempLrcLine, lrcLine);
            fprintf(tempFile, tempLrcLine); /* (w) tagging working, now to call the function to get the time elapsed */
        }
        else
            fprintf(tempFile, lrcLine);
        lineCounter++;
    }

    fclose(inpLrcFile);
    fclose(tempFile);

    inpLrcFile = fopen(inpFileName, "w");
    tempFile = fopen(TEMP_FILE, "r");

    /* (w) re-write the whole lyrics file from the tempfile */
    while((readTempLine = getline(&tempLine, &lenTempLine, tempFile)) != EOF)
        /* (w) read the tempFile and then rewrite the lyrics file */
        fprintf(inpLrcFile, tempLine);

    fclose(inpLrcFile);
    fclose(tempFile);

    free(lrcLine);
    free(tempLine);
}

void _getTime(char *inpCommand)
{
    FILE *pipeFHandler;
    char tempOut[MAXLINE];

    /* (w) this function will try to run the specified command and return the result - over-loaded from the void param type of runXtermCmd */
    pipeFHandler = popen(inpCommand, "r"); /* (w) only use the read mode of the process open function */
    if(pipeFHandler == NULL)
        printw("Could not run command : %s", inpCommand);
    else
        fgets(tempOut, sizeof(tempOut), pipeFHandler);

    /* (w) check which string has been provided to the function */
    if(strcmp(inpCommand, CURTIME) == 0)
        strcpy(cmdOut, tempOut); /* (w) This portion needs to be handled now */
    pclose(pipeFHandler);
}
