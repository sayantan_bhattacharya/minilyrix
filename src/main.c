/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Minilyrix main code
 *
 *        Version:  1.0
 *        Created:  Sunday 24 May 2015 04:29:48  IST
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  war10ck (),
 *   Organization:
 *
 * =====================================================================================
 */

#include "../inc/main.h"

/* (w) TODO: Add error handling to the code */
/* (w) TODO: Also try to use two different windows and display the window based on the appropriate mode */

int main(void)
{
    startMlx();
    return 0;
}
